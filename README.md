# Automation Test Package

This package uses the Cucumber.io release with the TestNG test runner.

## Developer setup.

1. Clone repo
2. Install Cucumber for Java plugin
3. Install Lombok
4. Enable annotation processing