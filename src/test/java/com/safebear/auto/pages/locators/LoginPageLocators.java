package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

/**
 * The @Data Lombok command is creating virtual getters and setters for the private variables.
 */
@Data
public class LoginPageLocators {

    //input fields
    private By usernameLocator = By.id("username");
    private By passwordLocator = By.xpath(".//input[@name='psw']");

    //buttons
    private By loginButonLocator = By.xpath(".//button[@id='enter']");

    //messages
    private By loginWarningLocator = By.xpath(".//p[@id='rejectLogin']/b");
}
