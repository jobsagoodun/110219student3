package com.safebear.auto.tests;


import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import static com.safebear.auto.utils.Utils.getDriver;
import static com.safebear.auto.utils.Utils.getUrl;
import static org.testng.Assert.*;
import static org.testng.Assert.assertEquals;

public class StepDefs {

    LoginPage loginPage;
    ToolsPage toolsPage;

    WebDriver driver;

    @Before
    public void setUp() {
        driver = getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);
    }

    @After
    public void tearDown() {
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.quit();
    }

    @Given("I navigate to the login page")
    public void i_navigate_to_the_login_page() {
        driver.get(getUrl());

        assertEquals(loginPage.getPageTitle(), "Login Page", "We're not on the Login Page or its title has changed");

    }

    @When("I enter the login details for a {string}")
    public void i_enter_the_login_details_for_a(String userType) {
        switch (userType) {
            case "invalidUser":
                loginPage.enterUsername("attacker");
                loginPage.enterPassword("dontletmein");
                loginPage.clickLoginButton();
                break;

            case "validUser":
                loginPage.enterUsername("tester");
                loginPage.enterPassword("letmein");
                loginPage.clickLoginButton();
                break;
            default:
                fail("The test data does not match the expected scenarios. The expected input is invalidUser or validUser. You entered: " + userType);
                break;
        }

    }


    @Then("I can see the following message: {string}")
    public void i_can_see_the_following_message(String validationMessage) {
        switch (validationMessage) {
            case "Username or Password is incorrect":
                assertTrue(loginPage.checkForFailedLoginWarning().contains(validationMessage));
                break;
            case "Login Successful":
                assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains(validationMessage));
                break;
            default:
                fail("The test data does not match the expected scenarios. The expected input is 'Username or Password is incorrect' or 'Login Successful'. You entered: " + validationMessage);
                break;
        }


    }

}
